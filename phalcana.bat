@echo off

rem Usage: phalcana [task] [opt1 opt2]
rem
rem And so on.
rem
rem To get help, pass in --help
rem
rem phalcana general help
rem phalcana --help
rem phalcana
rem
rem Task specific help
rem phalcana task:name --help

@setlocal

set TASK=""

if not "%ANSICON%" == "" set COLORS=--colors

if "%*"== "test" (

	"%PHALCANA_PATH%modules/unittest/vendor/bin/phpunit.bat" %COLORS% "--bootstrap=modules/unittest/index.php" "modules\unittest\tests.php"
	
	goto :eof
)

if not "%*"=="" set TASK=%*

set PHALCANA_PATH=%~dp0

"php" "%PHALCANA_PATH%index.php" %TASK%

@endlocal