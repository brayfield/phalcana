<?php

namespace Phalcana\Controllers;

use Phalcon\Mvc\View;

/**
 * undocumented class
 *
 * @package     Application
 * @category    Controllers
 * @author      Neil Brayfield
 */
class Base extends \Phalcon\Mvc\Controller
{

    /**
     * Setup general initialization rules
     *
     * @return  void
     **/
    public function initialize()
    {

        if ($this->request->isAjax()) {
            //$this->view->setRenderLevel(View::LEVEL_NO_RENDER);
            $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        }

        // CSS in the header
        $this->assets
            ->collection('cssHeader');

        // Javascripts in the header
        $this->assets
            ->collection('jsHeader')
            ->addJs('public/js/modernizr.js');

        // Javascripts in the footer
        $this->assets
            ->collection('jsFooter')
            ->addJs('public/js/app.js');

        // CSS in the footer
        $this->assets
            ->collection('cssFooter')
            ->addCss('public/css/app.css');


    }
}
