<?php

namespace Phalcana\Controllers;

/**
 * Home page controller
 *
 * @package     Application
 * @category    Controllers
 * @author      Neil Brayfield
 */
class IndexController extends Base
{

    /**
     * Initialize the controller
     *
     **/
    public function initialize()
    {

        parent::initialize();

    }


    /**
     * Index action
     *
     * @return  void
     **/
    public function indexAction()
    {


    }
}
