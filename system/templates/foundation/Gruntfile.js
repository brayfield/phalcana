/**
 * Gruntfile.js
 */
module.exports = function (grunt) {
	grunt.initConfig({
	pkg: grunt.file.readJSON('package.json'),

		sass: {
			default: {
				options: {
					includePaths: ['public/components/foundation/scss']
				},
				files: {
					'public/css/app.css': 'public/src/css/app.scss'
				}
			}
		},
		cmq: {
			default: {
				files: {
					'public/css/app.css': 'public/css/app.css'
				}
			},
		},
		autoprefixer: {
			options: {
				browsers: ['> 0%'],
				diff:true
			},
			default: {
				files: {

					'public/css/app.css': 'public/css/app.css'
				}
			}
		},
		cssmin: {
			default: {
				files: {
					'public/css/app.css': 'public/css/app.css'
				}
			}
		},


		import: {
			default: {
				files:  {
					'public/js/app.js': 'public/src/js/app.js'
				}
			}

		},

		uglify: {
			options: {
				sourceMap: true
			},
			default: {
				files: {
					'public/js/app.js': 'public/js/app.js',
					'public/js/modernizr.js' : 'public/components/modernizr/modernizr.js'
				}
			}
		},




		watch: {
			grunt: {
				files: ['Gruntfile.js'],
				tasks: ['build']
			},

			sass: {
				files: ['public/src/css/**/*.scss'],
				tasks: ['css']
			},
			js: {
				files: ['public/src/js/**/*.js'],
				tasks: ['js']
			},

			reload_files: {
				files: ['app/views/**/*.volt', 'public/js/**/*.js', 'public/css/**/*.css'],
				options: {
					livereload: true,
					spawn: false
				}
			}
		}

	});

	// load standard tasks
	grunt.loadNpmTasks('grunt-import');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-combine-media-queries');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ['watch']);



	grunt.registerTask('build', ['css','js']);

	grunt.registerTask('css', ['sass','cmq','autoprefixer', 'cssmin']);

	grunt.registerTask('js', ['import', 'uglify']);


};
